﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lobby
{
    public class AdultsWaitingRoom : WaitingRoom
    {
        protected override bool CanAddPlayer(Player p)
        {
            return base.CanAddPlayer(p) && p.IsMajor();
        }
    }
}
