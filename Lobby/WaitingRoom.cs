﻿namespace Lobby
{
    public class WaitingRoom
    {
        private List<Player> players = new List<Player>();

        public void AddPlayer(Player p)
        {
            if (CanAddPlayer(p))
            {
                players.Add(p);
                Console.WriteLine($"{p.Name} added.");
            }
            else
            {
                Console.WriteLine($"{p.Name} could not be added."); 
            }
        }

        protected virtual bool CanAddPlayer(Player p)
        {
            return !players.Contains(p);
        }

        public void RemovePlayer(Player p)
        {
            if (players.Contains(p))
            {
                players.Remove(p);
            }
        }

        public IReadOnlyCollection<Player> GetPlayers()
        {
            return players;
        }
    }
}