﻿namespace Lobby
{
    public struct Player
    {
        public int ID { get; }
        public string Name { get; }
        public int Age { get; }
        public bool IsMajor() { return Age >= 18; }
        public bool IsMinor() { return Age < 18; }
        
        
        public Player(int _id, string _name, int age)
        {
            ID = _id;
            Name = _name;
            Age = age;
        }
    }
}