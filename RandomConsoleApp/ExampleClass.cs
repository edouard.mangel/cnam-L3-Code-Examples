﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Lobby; 

namespace RandomConsoleApp
{
    internal class ExampleClass
    {
        private readonly WaitingRoom Room;

        public ExampleClass(WaitingRoom room)
        {
            Room = room;
        }

        internal void AddPlayer(Player player)
        {
            Room.AddPlayer(player);
        }
    }
}
