﻿// See https://aka.ms/new-console-template for more information

using Lobby;
using RandomConsoleApp;

WaitingRoom normalRoom = new WaitingRoom();
AdultsWaitingRoom adultRoom = new AdultsWaitingRoom();

ExampleClass normalExample = new ExampleClass(normalRoom);
ExampleClass adultExample = new ExampleClass(adultRoom);

Player Adult = new Player(1, "Adult", 19);
Player Minor = new Player(1, "Minor", 17);

normalExample.AddPlayer(Adult); 
normalExample.AddPlayer(Minor);

adultExample.AddPlayer(Adult);
adultExample.AddPlayer(Minor);

Console.WriteLine("Hello, World!");
